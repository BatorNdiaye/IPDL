<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"></meta>
		<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"></meta>
            	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"></meta>
		<link href="semantic/out/components/reset.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/site.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/container.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/grid.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/header.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/image.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/menu.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/divider.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/segment.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/form.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/input.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/button.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/list.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/message.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/icon.css" type="text/css" rel="stylesheet"></link>
		
		<style type="text/css">
		 body {
                      background-color: #DADADA;
                    }
		 .column{
			
			margin-left : 350px;
			margin-right : 350px;
			
		    }
		 

		</style>
		<title> Trajet Initial </title>
	</head>
	<body>
	<br><br><br>
		<div class="ui large fixed horizontal menu">
		  
		    <div class="right menu">
		    <div class="item">
			<a class="ui teal submit button" href="accueil1">Sign Out</a>
		    </div>
		    </div>
		  </div>
		<div class="ui large left fixed vertical menu">
		  <div class="item">
   			<center><h1 class="ui teal image header">
	    	    
		    <img class="ui large image" src="img/logo.PNG" ><br>
		    <div class="content">


                                    <b> Track'it</b>
                                  

                        </div>
			</h1></center>
		  </div>
		  <a class="item"  href="repTransporteurAccueil">Accueil</a>
		  
		  
		</div>
		</div>
		
				<div class="ui text container">
				<br><br>

		    <center><h2 class="ui header">

			<b>Trajet Initial</b>

		    </h2></center>
		    <div class="ui section divider"></div>
		    <h4 class="ui top attached block header">


			    Informations Personnelles
			  

		    </h4>
		    <div class="ui bottom attached segment">

			   <form class="ui form segment">
				<div class="field">
      					<label>Identifiant Bateau </label>
      					<input placeholder="identifiant" name="identifiant" type="text">
    				  </div>
				<div class="field">
      					<label>Nombre de Conteneurs </label>
      					<input placeholder="Nombre Conteneurs" name="nbre" type="text">
    				  </div>
				<div class="field">
      					<label>Port de Depart </label>
      					<input placeholder="Port Depart" name="Port Depart" type="text">
    				  </div>
				<div class="field">
      					<label>Nombre d'escales </label>
      					<input placeholder="nombre" name="nombre" type="text">
    				  </div>
				 <div class="field">
      					<label>Port d'Escale num 1 </label>
      					<input placeholder="Port d'Escale" name="Port d'Escale" type="text">
					<button class="ui icon button">
					  <i class="add icon"></i>
					</button>
    				  </div>
				<div class="field">
      					<label>Port d'Arrivee </label>
      					<input placeholder="Port d'arrivee" name="Port d'arrivee" type="text">
    				  </div>
				<div class="field">
      					<label>Duree Trajet </label>
      					<input placeholder="duree" name="duree" type="text">
    				  </div>
				<a class="ui teal submit button" href="repTransporteurAccueil">Valider</a>
			  </form>  
			
			  

		    </div>
		    
		    
		
		<p>
		</p>
		<p></p>
	</body>
</html>
