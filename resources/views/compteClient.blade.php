<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"></meta>
		<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"></meta>
            	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"></meta>
		<link href="semantic/out/components/reset.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/site.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/container.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/grid.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/header.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/image.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/menu.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/divider.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/segment.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/form.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/input.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/button.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/list.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/message.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/icon.css" type="text/css" rel="stylesheet"></link>
		<title> Compte Client </title>
		<style type="text/css">
		 body {
                      background-color: #DADADA;
                    }
		 .column{
			
			margin-left : 150px;
			margin-right : 150px;
			
		    }

		</style>
		
	</head>
	<body>
		<br><br><br>
		<div class="ui container">
		<div class="column">
		<center><h2 class="ui teal image header">
                       
                        <div class="content">


                                    <b>CREATION COMPTE CLIENT</b>
                                  

                        </div>
                </h2></center>
		<form class="ui form segment">
				  
				  <div class="field">
					<label> Civilite </label>
				  </div>
				  <div class="two fields">
				  <div class="field">
				      <div class="ui radio checkbox">
					<input name="frequency"  type="radio">
					<label>Mr</label>
				      </div>
				    </div>
				    <div class="field">
				      <div class="ui radio checkbox">
					<input name="frequency" type="radio">
					<label>Mme</label>
				      </div>
				    </div>
  				  </div>
    				  <div class="field">
      					<label>Prenom</label>
      					<input placeholder="Prenom" name="prenom" type="text">
    				  </div>
				  <div class="field">
				    <label>Nom</label>
				    <input placeholder="Nom" name="nom" type="text">
				  </div>
				  <div class="field">
				  	<label>Pseudo</label>
				    	<input placeholder="Pseudo" name="pseudo" type="text">
			          </div>
				  <div class="field">
					<label>CIN</label>
				    	<input placeholder="Numero d'identification" name="cin" type="text">
				  </div>
				  <div class="field">
					<label>Email</label>
				    	<input placeholder="email" name="email" type="email">
				  </div>
				  <div class="field">
				    <label>Mot de Passe</label>
				    <input name="password" type="password">
				  </div>
				  <div class="field">
				    <label>Confirmer mot de passe</label>
				    <input name="password" type="password">
				  </div>
				  <div class="field">
				    <label>Num Tel</label>
				    <input placeholder="Numero Tel" name="tel" type="text">
				  </div>
				  <div class="field">
				    <label>Adresse</label>
				    <input placeholder="Adresse" name="adresse" type="text">
				  </div>
				  <a class="ui right teal submit button" href="clientAccueil">Creer un compte</a>
				  
				</form>
				</div>
				</div>
	</body>
</html>
