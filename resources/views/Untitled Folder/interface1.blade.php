<!DOCTYPE html>
<html>
	<head>
  		<meta charset="utf-8">
		<title> Accueil </title>
		<!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">-->
		<link rel="stylesheet" type="text/css" href="semantic/out/semantic.min.css">
		<script src="{{asset('semantic/out/semantic.min.js')}}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	</head>
	<body>
		<!--<div class="ui container">-->
			<div class="ui sidebar vertical left inverted menu">
				<a href="#" class="item">
					
					Home
				</a>
				<a href="#" class="item">
					
					About
				</a>	
				<a href="#" class="item">
					
					Contact
				</a>	
				<a href="#" class="item">
					
					Sign In
				</a>	
			</div>
			<div class="ui basic icon top fixed menu">
				<a id="toggle" class="item">
					<i class="sidebar icon"></i>
					Menu
				</a>
			</div>
			<script>
				$('#toggle').click(function(){
					$('.ui.sidebar').sidebar('toggle');
				});
			</script>
		<!--</div>-->
	</body>
</html>
