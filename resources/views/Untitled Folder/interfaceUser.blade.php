<!--@extends('layouts.app')

@section('content')-->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Etat Civil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels --><!--Image Senegal-->
      <span class="logo-mini"><b>E</b>SP</span>
      <!-- logo for regular state and mobile devices --><!--Titre de l'appli-->
      <span class="logo-lg"><b>Site Officiel</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <!--Pousser le menu-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="img/avatar2.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Ndeye Bator Ndiaye</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="img/avatar2.png" class="img-circle" alt="User Image">

                <p>
                  Ndeye Bator Ndiaye 
                  <small>Member DOLPHIN</small>
                </p>
              </li>
              <!-- Menu Footer-->
	      <!--Phase de deconnexion-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="img/avatar2.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Ndeye Bator Ndiaye</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <center><b>DEMANDE DE DOCUMENTS D'ETAT CIVIL EN LIGNE</b></center>
      </h1>
	<hr/>
      <center><p>Le service de l'etat Civil ne peut fournir des actes d'etat-civil seulement si ces derniers ont ete etablis dans la ville.Vous avez la possibilite de faire sur le site <br> une demande d'acte de naissance, de mariage, de deces (envoye dans un delai de 02 jours).</p></center>
	<br>
	<center><p><b>La demande en ligne doit etre accompagnee d'une copie de la carte d'identite, du passeport ou du permis de conduire</b> </p></center>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
	<div class="col-lg-1"></div>
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Vos Coordonnees</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
		<!-- Select multiple-->
                <div class="form-group">
			  <label>Commune</label>
		          <select multiple class="form-control">
		            <option>Dakar</option>
		            <option>ile de Goree</option>
		            <option>Dakar-Plateau</option>
		            <option>Medina</option>
		            <option>Gueule Tapee, Fass, Colobane</option>
			    <option>Fann, PointE, Amitie</option>
		            <option>Grand Dakar</option>
		            <option>Biscuiterie</option>
		            <option>Hann Bel Air</option>
		            <option>Sicap, Liberte</option>
			    <option>Dieuppeul, Derkle</option>
		            <option>Grand Yoff</option>
		            <option>Mermoz, Sacre Coeur</option>
		            <option>Ouakam</option>
		            <option>Ngor</option>
                            <option>Yoff</option>
			    <option>Patte d'oie</option>
		            <option>Parcelles Assainies</option>
		            <option>Camberene</option>
		            <option>Guediawaye</option>
		            <option>Golf Sud</option>
			    <option>Yoff</option>
			    <option>Patte d'oie</option>
		            <option>Parcelles Assainies</option>
		            <option>Camberene</option>
		          </select>
                </div>
		 <!-- select -->
                <div class="form-group">
                  <label>Statut</label>
                  <select class="form-control">
		    <option>-Selectionner-</option>
                    <option>Mme</option>
                    <option>Mr</option>
                  </select>
                </div>
		<div class="col-lg-6">
			<div class="form-group">
		          <label>Nom (nom d'usage pour les femmes mariees)</label>
		          <input type="text" class="form-control" placeholder="Enter ...">
		        </div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
		          <label>Prenom</label>
		          <input type="text" class="form-control" placeholder="Enter ...">
		        </div>
		</div>
		<div class="form-group">
                  <label>Adresse</label>
                  <input type="text" class="form-control" placeholder="Enter ...">
                </div>
		<div class="col-lg-6">
			<div class="form-group">
		          <label>Ville</label>
		          <input type="text" class="form-control" placeholder="Enter ...">
		        </div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
		          <label>Pays</label>
		          <input type="text" class="form-control" placeholder="Enter ...">
		        </div>
		</div>
		<div class="form-group">
                  <label>Telephone</label>
                  <input type="text" class="form-control" placeholder="Enter ...">
                </div>
	        <!--Reprise-->
		<hr/>
		<div class="box-header with-border">
              		<h3 class="box-title">Vous etes : </h3>
            	</div>
               <div class="form-group">
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                     	Le titulaire de l'acte
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
			son conjoint
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" >
                      	son pere ou sa mere
                    </label>
                  </div>
		  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios4" value="option4" >
                      	son fils ou sa fille
                    </label>
                  </div>
	          <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios5" value="option5" >
                      	son grand-pere ou sa grand-mere
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios6" value="option7" >
                        Autre lien de parente
                    </label>
                  </div>
                  </div>
		  <hr/>
		  <div class="box-header with-border">
              		<h3 class="box-title">Quelle piece d'etat-civil souhaitez vous ?  </h3>
            	  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" >
                        Extrait
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" >
            		Copie Integrale
                    </label>
                  </div>
		  <div class="form-group">
                  	<label>Nombre de copies</label>
                  	<input type="text" class="form-control" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                  	<label>Document demande</label>
                  </div>
                   <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" >
                        Acte de naissance
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" >
            		Acte de mariage
                    </label>
                  </div>
		  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" >
            		Acte de deces
                    </label>
                  </div>
		  <div class="form-group">
                  	<label>Date de naissance, du mariage ou du deces</label>
                  </div>
		  <div class="form-group">
		  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                </div>
	        </div>
		  <div class="form-group">
                  	<label>Lieu de l'evenement</label>
                  	<input type="text" class="form-control" placeholder="Enter ...">
                  </div>
		  <div class="col-lg-6">
			  <div class="form-group">
		          	<label>Nom de naissance du titulaire de l'acte</label>
		          	<input type="text" class="form-control" placeholder="Enter ...">
		          </div>
		  </div>
                  <div class="col-lg-6">
		          <div class="form-group">
		          	<label>Prenom</label>
		          	<input type="text" class="form-control" placeholder="Enter ...">
		          </div>
		  </div>
		  
		
		  
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Envoyer La demande</button>
              </div>

	      
            </form>
          </div>
          
          
          <!-- general form elements disabled -->
          <!--<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">General Elements</h3>
            </div>-->
            <!-- /.box-header -->
            <!--<div class="box-body">
              <form role="form">
                <!-- text input -->
                <!--<div class="form-group">
                  <label>Text</label>
                  <input type="text" class="form-control" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Text Disabled</label>
                  <input type="text" class="form-control" placeholder="Enter ..." disabled>
                </div>

                <!-- textarea -->
                <!--<div class="form-group">
                  <label>Textarea</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>
                <div class="form-group">
                  <label>Textarea Disabled</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                </div>

                <!-- input states -->
                <!--<div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Input with success</label>
                  <input type="text" class="form-control" id="inputSuccess" placeholder="Enter ...">
                  <span class="help-block">Help block with success</span>
                </div>
                <div class="form-group has-warning">
                  <label class="control-label" for="inputWarning"><i class="fa fa-bell-o"></i> Input with
                    warning</label>
                  <input type="text" class="form-control" id="inputWarning" placeholder="Enter ...">
                  <span class="help-block">Help block with warning</span>
                </div>
                <div class="form-group has-error">
                  <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with
                    error</label>
                  <input type="text" class="form-control" id="inputError" placeholder="Enter ...">
                  <span class="help-block">Help block with error</span>
                </div>

                <!-- checkbox -->
                <!--<div class="form-group">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                      Checkbox 1
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                      Checkbox 2
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" disabled>
                      Checkbox disabled
                    </label>
                  </div>
                </div>

                <!-- radio -->
               <!-- <div class="form-group">
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                      Option one is this and that&mdash;be sure to include why it's great
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                      Option two can be something else and selecting it will deselect option one
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                      Option three is disabled
                    </label>
                  </div>
                </div>

                <!-- select -->
                <!--<div class="form-group">
                  <label>Select</label>
                  <select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>
                

                <!-- Select multiple-->
               <!-- <div class="form-group">
                  <label>Select Multiple</label>
                  <select multiple class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>
               

              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>UCAD | ESP | DGI | DIC1 INFO &copy; 2016-2017 <a href="#">Team Dolphin</a>.</strong> Tous droits reserves.
  </footer>

 

<!-- jQuery 2.2.3 -->
<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('js/app.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('js/demo.js')}}"></script>

<script src="{{asset('plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
</body>
</html>
<!--@endsection-->
