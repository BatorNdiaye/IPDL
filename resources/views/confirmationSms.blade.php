<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"></meta>
		<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"></meta>
            	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"></meta>
		<link href="semantic/out/components/reset.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/site.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/container.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/grid.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/header.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/image.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/menu.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/divider.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/segment.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/form.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/input.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/button.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/list.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/message.css" type="text/css" rel="stylesheet"></link>
		    <link href="semantic/out/components/icon.css" type="text/css" rel="stylesheet"></link>
		
		<style type="text/css">
		 body {
                      background-color: #DADADA;
                    }
		 .column{
			
			margin-left : 350px;
			margin-right : 350px;
			
		    }

		</style>
		<title> Confirmation SMS </title>
	</head>
	<body>
	<br><br><br>
	<div class="ui container">
		<div class="column">
			<center><h2 class="ui teal image header">
                        
                        	<div class="content">


                                    <b>CODE CONFIRMATION SMS</b>
                                  

                        	</div>
                	</h2></center>
			<form class="ui form segment">
				<div class="field">
      					<label>Veuiller entrer le code de confirmation recu par sms</label>
      					<input placeholder="Code" name="code" type="text">
    				  </div>
				  <a class="ui right teal submit button" href="tracking">confirmer</a>
			</form>
		</div>
	</div>
	</body>
</html>
