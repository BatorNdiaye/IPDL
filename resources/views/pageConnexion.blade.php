<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8"></meta>
            <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"></meta>
            <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"></meta>
            <title>Page de connexion</title>
            <link href="semantic/out/components/reset.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/site.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/container.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/grid.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/header.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/image.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/menu.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/divider.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/segment.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/form.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/input.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/button.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/list.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/message.css" type="text/css" rel="stylesheet"></link>
            <link href="semantic/out/components/icon.css" type="text/css" rel="stylesheet"></link>
            <script src="assets/library/jquery.min.js"></script>
            <script src="semantic/out/components/form.js"></script>
            <script src="semantic/out/components/transition.js"></script>
            <style type="text/css">
		 body {
                      background-color: #DADADA;
                    }

		 .column{
			
			margin-left : 350px;
			margin-right : 350px;
			
		    }

            </style>
            <script>
                  $(document)
                    .ready(function() {
                      $('.u…
            </script>
        </head>
        <body>
	    <br><br><br><br>
	    <div class="ui container">	
            <div class="ui middle aligned center aligned grid">
               <div class="column">
                    <h2 class="ui teal image header">
                        
                        <div class="content">


                                    <b>Track'it</b>
                                  

                        </div>
                    </h2>
                    <form class="ui large form">
                        <div class="ui stacked segment">
                            <div class="field">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input type="text" placeholder="Pseudo" name="pseudo"></input>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui left icon input">
                                    <i class="lock icon"></i>
                                    <input type="password" placeholder="Password" name="password"></input>
                                </div>
                            </div>
			    <div class="field">
			    <div class="ui checkbox">
				<input class="hidden" tabindex="0" type="checkbox">
      				<label>Se souvenir de moi</label>
			    </div>
			    </div>
                            
				<a class="ui fluid large teal submit button" href="clientAccueil">Log in</a>
                               

                           
			    <br>
			    
				<a class="ui fluid large teal submit button" href="compteClient">Creer un compte</a>
                                

                            
                        </div>
                        <div class="ui error message"></div>
                    </form>
                    <div class="ui message">


                               

                        <a href="changerMdp">

                            <u>Mot de passe oublie</u>

                        </a>
                    </div>
                </div>
            </div>
	</div>
        </body>
    </html>


