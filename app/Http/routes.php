<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/' function() {
	return view('welcome');
});*/


Route::get('/',function() {
	return view('index');
});

Route::get('/interfaceUser', function () {
    return view('interfaceUser');
});


Route::get('/interface1', function () {
    return view('interface1');
});

Route::get('/compteClient',function() {
	return view('compteClient');
});

Route::get('/accueil',function() {
	return view('accueil');
});


Route::get('/suiviSansCompte',function() {
	return view('suiviSansCompte');
});

Route::get('/confirmationSms',function() {
	return view('confirmationSms');
});


Route::get('/clientSuivi',function() {
	return view('clientSuivi');
});


Route::get('/compteTransporteur',function() {
	return view('compteTransporteur');
});


Route::get('/clientPremier',function() {
	return view('clientPremier');
});

Route::get('/transporteurAccueil',function() {
	return view('transporteurAccueil');
});

Route::get('/pageTransporteur',function() {
	return view('pageTransporteur');
});

Route::get('/pageRepTransporteur',function() {
	return view('pageRepTransporteur');
});

Route::get('/contratTransporteur',function() {
	return view('contratTransporteur');
});

Route::get('/parametreTransporteur',function() {
	return view('parametreTransporteur');
});

Route::get('/transporteurChangerMdp',function() {
	return view('transporteurChangerMdp');
});

Route::get('/transporteurChangerNumeroTel',function() {
	return view('transporteurChangerNumeroTel');
});

Route::get('/transporteurAvisClient',function() {
	return view('transporteurAvisClient');
});

Route::get('/annonceTransporteur',function() {
	return view('annonceTransporteur');
});

Route::get('/suiviTransporteur',function() {
	return view('suiviTransporteur');
});

Route::get('/repTransporteurAccueil',function() {
	return view('repTransporteurAccueil');
});

Route::get('/trajetRepresentant',function() {
	return view('trajetRepresentant');
});

Route::get('/positionRepresentant',function() {
	return view('positionRepresentant');
});

Route::get('/parametreRepresentant',function() {
	return view('parametreRepresentant');
});

Route::get('/changerMdpRepresentant',function() {
	return view('changerMdpRepresentant');
});

Route::get('/compteRepresentantTransporteur',function() {
	return view('compteRepresentantTransporteur');
});

Route::get('/changerNumeroTelRepresentant',function() {
	return view('changerNumeroTelRepresentant');
});



Route::get('/Page1',function() {
	return view('Page1');
});

Route::get('/clientAccueil',function() {
	return view('clientAccueil');
});

Route::get('/pageConnexion',function() {
	return view('pageConnexion');
});

Route::get('/changerNumeroTel',function() {
	return view('changerNumeroTel');
});

Route::get('/changerMdp',function() {
	return view('changerMdp');
});

Route::get('/Parametre',function() {
	return view('Parametre');
});

Route::get('/changerItineraire',function() {
	return view('changerItineraire');
});

Route::get('/avisClient',function() {
	return view('avisClient');
});

Route::get('/historique',function() {
	return view('historique');
});

Route::get('/tracking',function() {
	return view('tracking');
});

Route::get('/validerContrat',function() {
	return view('validerContrat');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');





